#ifndef __MMU__
#define __MMU__

#include "idt.h"

typedef struct Directory_Entry_s {
    unsigned int exists : 1;
    unsigned int read_write : 1;
    unsigned int privilege_level : 1;
    unsigned int cache_write_method : 1;
    unsigned int cache_disabled : 1;    
    unsigned int is_used : 1;    
    unsigned int : 1;    
    unsigned int page_size : 1;    
    unsigned int : 4;    
    unsigned int phys_page : 20;    
} Directory_Entry_s;

typedef struct Page_Entry_s {
    unsigned int exists : 1;
    unsigned int read_write : 1;
    unsigned int privilege_level : 1;
    unsigned int cache_write_method : 1;
    unsigned int cache_disabled : 1;
    unsigned int is_accessed : 1;
    unsigned int dirty_bit : 1;
    unsigned int : 1;
    unsigned int is_global : 1;
    unsigned int : 3;
    unsigned int phys_page : 20;
} Page_Entry_s;

typedef struct CR_0 {
    unsigned int : 15;
    unsigned int kernel_ro : 1;
    unsigned int : 15;
    unsigned int pg : 1;
} CR_0;

typedef unsigned int CR_3;

void setup_mmu();
void page_fault_handler(int_regs_t *r);

#endif