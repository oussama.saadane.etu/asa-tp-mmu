#include "mmu.h"
#include "idt.h"


#define PAGE_SIZE 4096

Directory_Entry_s page_directory[1024] __attribute__((aligned(PAGE_SIZE)));
Page_Entry_s page_entry[1024] __attribute__((aligned(PAGE_SIZE)));
Page_Entry_s user_page_table[1024] __attribute__((aligned(PAGE_SIZE)));

extern void kputs(char* str);
extern void puthex(unsigned aNumber);
extern char _begin_of_user_space;
extern char _end_of_user_space;

int first_free_page = 2046;
int reserved_pages_index[16 * 1024 * 1024 / PAGE_SIZE];

void *page_to_addr(int page) {
    return (void *)(page * PAGE_SIZE);
}

int addr_to_page(void *addr) {
    return (int) addr / PAGE_SIZE;
}

void *alloc_page() {
    int old_first_free_page = first_free_page;

    user_page_table[old_first_free_page - 1024].exists = 0b1;

    first_free_page = reserved_pages_index[old_first_free_page];
    reserved_pages_index[old_first_free_page] = -1;
    return page_to_addr(old_first_free_page);
}

void free_page(void *addr) {
    int page_to_free = addr_to_page(addr);
    reserved_pages_index[page_to_free] = first_free_page;
    first_free_page = page_to_free;
}

void setup_mmu() {
    const int first_user_page_nb = addr_to_page(&_begin_of_user_space);
    const int last_user_page_nb = addr_to_page(&_end_of_user_space);

    // reserved_pages_index INIT
    for (int i = 16 * 1024 * 1024 / PAGE_SIZE - 1; i != 0; i--) {
        if (i == 2047) {
            reserved_pages_index[i] = -1;
        }
        else if (i < 1024 && i != 0) {
            reserved_pages_index[i] = -1;
        }
        else if (first_user_page_nb <= i && i <= last_user_page_nb) {
            reserved_pages_index[i] = -1;
        }
        else {
            reserved_pages_index[i] = i - 1;
        }
    }

    for (int i = 0; i < 1024; i++) {
        page_directory[i].exists = 0;
    }

    // memory for kernel
    for (int i = 0; i < 1024; i++) {
        page_entry[i].exists = 1;
        page_entry[i].phys_page = i;
    }

    // memory for userland
    for (int i = 0; i < 1024; i++) {
        if (i < (last_user_page_nb - first_user_page_nb) + 1) {
            user_page_table[i].exists = 1;
        }
        else {
            user_page_table[i].exists = 0;
        }
        user_page_table[i].phys_page = (unsigned int) first_user_page_nb + i;
        user_page_table[i].privilege_level = 1;
        user_page_table[i].read_write = 1;
    }
    user_page_table[1023].exists = 1;

    // stack guard
    const int l_page = last_user_page_nb - first_user_page_nb + 1;
    user_page_table[l_page].exists = 1;
    user_page_table[l_page].privilege_level = 1;
    user_page_table[l_page].read_write = 0;

    // a sooner stack guard
    const int one_mega_stack_page = addr_to_page((void *) 0x700000) - 1024;
    user_page_table[one_mega_stack_page].exists = 1;
    user_page_table[one_mega_stack_page].privilege_level = 1;
    user_page_table[one_mega_stack_page].read_write = 0;


    // idx1 = 0
    page_directory[0].exists = 1;
    page_directory[0].phys_page = (unsigned int) &page_entry >> 12;

    // idx1 = 1
    page_directory[1].exists = 1;
    page_directory[1].phys_page = (unsigned int) &user_page_table >> 12;
    page_directory[1].privilege_level = 1;
    page_directory[1].read_write = 1;

    CR_0 cr0;
    CR_3 cr3;
    cr3 = (unsigned int) &page_directory & 0b11111111111111111111000000000000;

    __asm__ ("mov %0, %%cr3" : : "r"(cr3));
    __asm__ ("mov %%cr0, %0" : "=r" (cr0));
    cr0.pg = 1;
    cr0.kernel_ro = 1;
    __asm__("mov %0, %%cr0" : : "r"(cr0));
}

void *get_cr2() {
    void* cr2;
    __asm__ ("mov %%cr2, %0" : "=r" (cr2));
    return cr2;
}

void page_fault_handler(int_regs_t *r) {
    /* 
        Code 7 : usermode, page existante, write interdit
        Code 6 : usermode, page inexistante
    */

    void *cr2 = get_cr2();
    if (r->err_code == 6) {
        int addr = (int) alloc_page();
        if (addr < 0) {
            kputs("Impossible to allocate more memory");
            for(;;);
        }
        
    }
    else {
        if (r->err_code == 7) {
            kputs("\nStack overflow ! (code 7)");
        }
        else{
        kputs("ERROR ! Page fault...\n Code : ");
        puthex(r->err_code);
        }
        kputs(" EIP : 0x");
        puthex(r->eip);
        kputs(" CR2 : 0x");
        puthex((unsigned int) cr2);
        kputs(" Fault page :");
        puthex((int) addr_to_page(cr2));
        kputs("\n");
        for(;;);
    }
    
   
}


// L'amour toujours