// static void putc(int c) {
// 	asm("movl $1, %%eax" "\n\t" "movl %0, %%edi" "\n\t" "int $87" : : "r"(c) : "eax", "edi");
// }
#include "user.h"

// char user_stack[USER_STACK_SIZE] __attribute__((aligned(4096))); // old_char

void syscall_caller(unsigned int no, void *arg) {
    asm("mov %0, %%edx" : : "r"(arg) : "eax", "edx");
    asm("mov %0, %%eax" : : "r"(no) : "eax", "edx");
    asm("int $87");
}

void puts(char *s) {
    syscall_caller(0, (void *) s);
}

void uclear_screen() {
    syscall_caller(1, '\0');
}

void user_main() {
    void recurscv() {
        return recurscv();
    }
    recurscv();
}

