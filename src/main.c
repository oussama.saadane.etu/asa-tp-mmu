#include "ioport.h"
#include "gdt.h"
#include "idt.h"
#include "mmu.h"
#include "call_user.h"
#include "user.h"

void clear_screen();				/* clear screen */
void putc(char aChar);				/* print a single char on screen */
void kputs(char *aString);			/* print a string on the screen */
void puthex(unsigned aNumber);			/* print an Hex number on screen */
void syscall_hander(int_regs_t *r);

void empty_irq(int_regs_t *r) {
}

extern char user_stack[USER_STACK_SIZE];

/* multiboot entry-point with datastructure as arg. */
void main(unsigned int * mboot_info)
{
    /* clear the screen */
    clear_screen();
    kputs("Early boot.\n"); 
    kputs("\t-> Setting up the GDT... ");
    gdt_init_default();
    kputs("OK\n");

    kputs("\t-> Setting up the IDT... ");
    setup_idt();
    kputs("OK\n");

    kputs("\n\n");

    idt_setup_irq_handler(0, empty_irq);
    idt_setup_irq_handler(1, empty_irq);
    idt_setup_int_handler(14, page_fault_handler);
    idt_setup_int_handler(87, syscall_hander);

    __asm volatile("sti");

    /* minimal setup done ! */

    setup_mmu();
    // call_user(&user_main,user_stack + USER_STACK_SIZE - 4);
    call_user(&user_main,(void *)0x800000); // cf Q34
    kputs("Going idle\n");
    for(;;) ; /* nothing more to do... really nothing ! */
}
void syscall_hander(int_regs_t *r) {
  const unsigned int syscall_no = (const unsigned int) r->eax;
  const unsigned int arg = (const unsigned int) r->edx;
  switch (syscall_no) {
    case 0:
      kputs((char*) arg);
      break;
    case 1:
      clear_screen();
      break;
    case 2:
      puthex((unsigned int) arg);
      break;
  }
  }

/* base address for the video output assume to be set as character oriented by the multiboot */
unsigned char *video_memory = (unsigned char *) 0xB8000;

/* clear screen */
void clear_screen() {
  int i;
  for(i=0;i<80*25;i++) { 			/* for each one of the 80 char by 25 lines */
    video_memory[i*2+1]=0x0F;			/* color is set to black background and white char */
    video_memory[i*2]=(unsigned char)' '; 	/* character shown is the space char */
  }
}

/* print a string on the screen */
void kputs(char *aString) {
  char *current_char=aString;
  while(*current_char!=0) {
    putc(*current_char++);
  }
}

/* print an number in hexa */
char *hex_digit="0123456789ABCDEF";
void puthex(unsigned aNumber) {
  int i;
  int started=0;
  for(i=28;i>=0;i-=4) {
    int k=(aNumber>>i)&0xF;
    if(k!=0 || started) {
      putc(hex_digit[k]);
      started=1;
    }
  }
  if(!started) putc('0');
}

/* print a char on the screen */
int cursor_x=0;					/* here is the cursor position on X [0..79] */
int cursor_y=0;					/* here is the cursor position on Y [0..24] */

void setCursor() {
  int cursor_offset = cursor_x+cursor_y*80;
  _outb(0x3d4,14);
  _outb(0x3d5,((cursor_offset>>8)&0xFF));
  _outb(0x3d4,15);
  _outb(0x3d5,(cursor_offset&0xFF));
}

void putc(char c) {
  if(cursor_x>79) {
    cursor_x=0;
    cursor_y++;
  }
  if(cursor_y>24) {
    cursor_y=0;
    clear_screen();
  }
  switch(c) {					/* deal with a special char */
    case '\r': cursor_x=0; break;		/* carriage return */
    case '\n': cursor_x=0; cursor_y++; break; 	/* new ligne */	
    case 0x8 : if(cursor_x>0) cursor_x--; break;/* backspace */
    case 0x9 : cursor_x=(cursor_x+8)&~7; break;	/* tabulation */
						/* or print a simple character */
    default  : 
      video_memory[(cursor_x+80*cursor_y)*2]=c;
      cursor_x++;
      break;
  }
  setCursor();
}


