Ce fichier est le rendu de <PRENOM> <NOM> & <PRENOM> <NOM>
==========================================================

Définir des structures de données MMU
-------------------------------------




Définir un plan d'adressage
---------------------------

## Q5 Adresse de compilation

Le code compilé (c-à-d la section .text) est placée après l'adresse phys = 0x00100000 de manière à tomber sur le premier multiple de 4096. (ALIGN(4096))
Ensuite les sections .data, .bss, .stack sont placées succesivement sur des multiples de 4096.

## Q6 Mémoire virtuelle et noyau

### my-kernel

0x00100000 est multiple de 4096, c'est l'adresse où le kernel est placé.


idx1 = b0000000000
idx2 = b0100000000

### video_memory

Adresse : 0xB8000
idx1 = b0000000000
idx2 = b0010111000

## Q7 Espace mémoire

Une seule entrée du Page Table Directory est configurée (idx 2). La Page Table correspondante indexe 2¹⁰ = 1024 pages.

Donc 1024 pages sont configurées.
Cela correspond à $1024 \times 4096 = 4194304  octets$

## Q8 Plan d'adressage

+----------------------+
¦ Page Table Directory ¦
+----------------------+       +--------------------------+
¦ idx1 : b0000000000   ¦ ----> ¦ Page Table               ¦
+----------------------+       +==========================+
                               ¦ idx2        ¦ phys       ¦
                               +--------------------------+
                               ¦b0000000000  ¦ 0x00100000 ¦
                               ¦b0000000001  ¦ 0x00100001 ¦
                               ¦etc...                    ¦
                               +-------------+------------+

Un premier programme _user_
---------------------------

## Q12 Plan d'adressage _user_

Plus petite adresse accessible :
b0000000001 0000000000 000000000000
    |            |          |
    V            V          V
   idx1         idx2       offset   

Plus grande adresse accesible :
b0000000001 1111111111 111111111111

## Question Premier programme _user_ #6

Une erreur est produite pendant l'accès à puts. Car la fonction est définie en dehors de l'espace d'adressage.

Le registre CR2 nous donne l'addresse accédée qui provoque une faute.

Pour savoir que cela correspond à la fonction `puts`, on désassemble l'iso avec la commande suivante :
`objdump -d ./iso/boot/mykernel.iso`

## Q22 Interposition d'appel

Les tâches sensibles ne sont effectuées qu'en mode kernel.
Toutes les syscall sont gérés au même endroit et l'utilisateur ne peut ajouter les siennes.

## Q29 Analyse
TODO
mots-clés : dans mmu.c; moins sûr car plus ahut niveau; contrôle plus bas niveau.

## Q34 Pile d'appel virtuelle

On veut idx1 et idx2 qui "pointent" vers la dernière page de usertable, mais incrémenté de 1 car push décrémentera après cela on se trouvera au début l'espace voulu :
idx1 = b0000000010
idx2 = b0000000000
offset = 0